#!/bin/bash
DIALOG=${DIALOG=dialog}

################################################################################################################

# Variables

################################################################################################################

TITLEARCH="PwOSS - Server | Raspberry - Archlinux | ARM - (Script)"
BACKTITLE="PwOSS - Server | Raspberry - Archlinux | ARM - (Script)"
CANCEL="Stop the script"
all="EVERYTHING"
raspi="wget https://github.com/haiwen/seafile-rpi/releases/download/v6.3.4/seafile-server_6.3.4_stable_pi.tar.gz"
#WMB="dialog --title "$TITLE" --msgbox \"\ "
#SIZE="$WT_HEIGHT $(( $WT_WIDTH / 2 )) $WT_MENU_HEIGHT"
#############END############

################################################################################################################

# Script using as root

################################################################################################################

if [ "$EUID" -ne 0 ]
	then $DIALOG --title "sudo sh PwOSS-Server_Raspberry-Archlinux-ARM-(Script).sh" --backtitle "$BACKTITLE" --msgbox "It doesn't work without root. Use sudo instead. Thanks." 8 78
clear;
				exit
fi
#############END############

################################################################################################################

# First Time Boot?

################################################################################################################
#confirmAnswer () {
#$DIALOG --title "First Time Boot?" --backtitle "$BACKTITLE" --yes-button "Yes" --no-button "No"  --defaultno --yesno "$1" 10 56
#return $?
#}
#confirmAnswer "Do you use the Image the firt time?"
#if [ $? = 0 ]; then
#pacman -Sy archlinux-keyring && pacman-key --init && pacman-key --populate archlinuxarm && pacman -Syu --noconfirm;
#fi
#############END############

################################################################################################################

# Optimal size for whiptail/dialog

################################################################################################################

calc_wt_size() {
  WT_HEIGHT=24
  WT_WIDTH=$(tput cols)

  if [ -z "$WT_WIDTH" ] || [ "$WT_WIDTH" -lt 60 ]; then
    WT_WIDTH=80
  fi
  if [ "$WT_WIDTH" -gt 178 ]; then
    WT_WIDTH=120
  fi
  WT_MENU_HEIGHT=$($WT_HEIGHT-6)
}
calc_wt_size
#############END############

################################################################################################################

# Info Box

################################################################################################################

$DIALOG --title "Important" --backtitle "$BACKTITLE" --msgbox "\
Important - before you start check following:

		1. Your router needs the possibility of port forwarding and the possibility to configure the DNS server for LAN devices.
		2. You’ll need a DynDNS-Domain. For example, at https://www.noip.com/sign-up.
		3. You have to connect a USB Stick/External HardDrive

The following steps will guide you through the script to install the server at your home.
" $WT_HEIGHT $(( $WT_WIDTH / 1 )) $WT_MENU_HEIGHT
#############END############

################################################################################################################

# Update System

################################################################################################################

confirmAnswer () {
$DIALOG --title "Update" --backtitle "$BACKTITLE" --yes-button "Yes" --no-button "No"  --defaultno --yesno "$1" 10 56
return $?
}
confirmAnswer "Let's update the system first."
if [ $? = 0 ]; then
pacman -Syu --noconfirm;
fi
#############END############

################################################################################################################

# Software Selection

################################################################################################################

O=$($DIALOG --title "PwOSS - Minimal Script" --backtitle "$BACKTITLE" --checklist \
"Choose following option. (use space)" $WT_HEIGHT $(( $WT_WIDTH / 1 )) 11 \
"First time user?" "Get everything installed." Off \
"What's the Raspberry IP Address?" "Raspberry IP Address" OFF \
"VPN client" "Create another VPN Client" OFF \
"Radicale user" "Add another user for your contact & calendar server" OFF \
"dm-crypt LUKS" "Add another key to your ENRCYPTED USB STICK or HardDrive" OFF \
"Seafile" "Update the Seafile server" OFF \
"Seafile User" "Add another user for your cloud server" OFF \
"AUR Helper - 'yay'" "Update the AUR Software (ovpngen,libsepol,libselinux ...)" OFF \
"Change passwords and ports" "User, VPN, UFW ..." OFF \
"Something went wrong?" "Every stop are listed." OFF \
"STOP Script" "Stop the script every boot." OFF 3>&1 1>&2 2>&3)

############
#libsepol and libselinux for seafile update through yay needs to be check. If updates are available we have to change the architecture in armv7h - arch=('any').
############

exitstatus=$?
if [ $exitstatus = 0 ]
then
$DIALOG --title "Message" --backtitle "$BACKTITLE" --yesno "You selected $O" --yes-button "Confirm" --no-button "Back" 10 50
else
$DIALOG --title "Thankyou" --backtitle "$BACKTITLE" --msgbox "See You soon" 10 50
clear;
fi
#############END############
