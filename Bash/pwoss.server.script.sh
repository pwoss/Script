#!/bin/bash

WIDTH=50
HEIGHT=10

install_arch() {
  dialog \
  --title "archlinux installation" \
  --msgbox "Welcome to the archlinux PwOSS installation" "${HEIGHT}" "${WIDTH}"
}

install_arm() {
  dialog \
  --title "raspberry pi installation" \
  --msgbox "Welcome to the raspberry pi PwOSS installation" "${HEIGHT}" "${WIDTH}"
}

dialog_is_installed() {
  __DIALOG=`pacman -Q dialog | wc -l`
  if [ ${__DIALOG} == 0]; then 
    return false 
  else 
    return true
  fi
}

linux_install() { 
  MACHINE_TYPE=`uname -m`
  if [ ${MACHINE_TYPE} == 'x86_64' ]; then
    install_arch
  else
    install_arm
  fi
}

macos_install() { 
  echo "Platform not supported"
  exit 
}

main() {
  #
  MACHINE="$(uname -s)"
  case "${MACHINE}" in
    Linux*) linux_install;;
    Darwin*) macos_install;;
    *) exit
  esac
  
  clear
  exit
  # Check if dialog is installed
  #if dialog_is_installed; then
  #  echo "success"
 
  #MACHINE_TYPE=`uname -m`
  #if [ ${MACHINE_TYPE} == 'x86_64' ]; then
  #  install_arch
  #else
  #  install_arm
  #fi
}

main
