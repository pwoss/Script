from dialog import Dialog
import os, time

class Screen:
    def __init__(self, dialog, wizard=None):
        self.dialog = dialog
        self.wizard = wizard

        self.width = 80
        self.height = 20
        self.line_height = 7

    def display(self):
        ### OVERRIDDEN ###
        pass

class Welcome(Screen):
    def __init__(self, dialog, wizard):
        super().__init__(dialog, wizard)    

        self.text= R"""

        1. You need a router capable of port forwarding and able to configure a DNS server
        2. You’ll need a DynDNS-Domain. For example, at https://www.noip.com/sign-up
        3. You have to connect a USB Stick/External HardDrive

        Get the image from https://www.archlinuxarm.org and follow the instructions.
        """    
        
    def display(self):
        code, tags = self.dialog.checklist(self.text, self.height, self.width, self.line_height, 
        choices=[("Automated", "First time users only", False)], 
        title="Important! - Before you start check following:")

        # Handle automated / advanced configuration
        if code == self.dialog.OK and tags and tags[0] == "Automated":
            self.wizard.active = 1
        elif code == self.dialog.OK:
            self.wizard.active = 2
        if code == self.dialog.CANCEL: 
            self.wizard.is_running = False 

class Automated(Screen):
    def __init__(self, dialog, wizard):
        super().__init__(dialog, wizard)
        self.text = """Choose your packages:"""
        
    def display(self):
        
        self.wizard.install(automated=True)

        
        msg = "Packages Installed:\n\n"


        for package in self.wizard.packages:
            msg += " -- " + package[0].upper() + '\n'
        
        code = self.dialog.scrollbox(msg, self.height, self.width, title="Configuration Finished")

        if code == self.dialog.OK:
            self.wizard.is_running = False

class Advanced(Screen):
    def __init__(self, dialog, wizard):
        super().__init__(dialog, wizard)
        self.text = """Choose your packages:"""
        
    def display(self):
        #
        code, tags = self.dialog.checklist(
            self.text, 
            self.height,
            self.width,
            self.line_height+10,
            choices=self.wizard.packages,
            title="Advanced"
        )
    
        if code == self.dialog.OK:
            for i in [i for i, p in enumerate(self.wizard.packages) for tag in tags if tag in p]:
                temp = list(self.wizard.packages[i])
                temp[2] = True
                self.wizard.packages[i] = tuple(temp)
            self.wizard.install()
        elif code == self.dialog.CANCEL:
            self.wizard.active = 0

        code = self.dialog.scrollbox("", self.height, self.width, title="Configuration Finished")

        if code == self.dialog.OK:
            self.wizard.is_running = False

class Wizard:
    #
    def __init__(self, dialog):
        self.dialog = dialog
        self.is_running = True
        self.screens = []
        self.active = 0

        self.dialog.set_background_title("PwOSS Server Configuration")

        self.packages = [
            ("Radicale", "Contact and Calender Server", False),
            ("Seafile",  "Cloud Server",                False),
            ("WebDav",   "WebDav Server",               False),
            ("VPN",      "Virtual Private Network",     False),
            ("Samba",    "Windows File Server",         False),
            ("Firefox",  "Sync Bookmarks | History",    False),
            ("Pi-Hole",  "Advertising Blocker",         False),
            ("FreshRSS", "RSS Reader",                  False)
        ]
        self.installs = {
            "Radicale": self.__radicle,
            "Seafile":  self.__seafile,
            "WebDav":   self.__webdav,
            "VPN":      self.__vpn,
            "Samba":    self.__samba,
            "Firefox":  self.__firefox,
            "Pi-Hole":  self.__pihole,
            "FreshRSS": self.__freshrss
        }
        self.log = ""

    def install(self, automated=False):
        self.dialog.gauge_start("Loading", 20, 80, 0, title="Configuration in Progress")

        count = 0
        if automated:
            for key, value in self.installs.items():
                self.dialog.gauge_update(count*10, value(), update_text=True)
                count += 1
                time.sleep(1)
        else:
            for p in [package for package in self.packages if package[2]]:                
                self.dialog.gauge_update(count*10, self.installs[p[0]](), update_text=True)
                count += 1
                time.sleep(1)

        self.dialog.gauge_update(100, "Configuration Finished")
        self.dialog.gauge_stop()


    ##
    def __radicle(self):
        #self.dialog.msgbox("Installing Radicle")
        return "Installing Radicle"

    def __seafile(self):
        #self.dialog.msgbox("Installing Seafile")
        return "Installing Seafile"

    def __webdav(self):
        #self.dialog.msgbox("Installing WebDav")
        return "Installing WebDav"

    def __vpn(self):
        #self.dialog.msgbox("Installing VPN")
        return "Installing VPN"

    def __samba(self):
        #self.dialog.msgbox("Installing Samba")
        return "Installing Samba"

    def __firefox(self):
        #self.dialog.msgbox("Installing Firefox")
        return "Installing Firefox"

    def __pihole(self):
        #self.dialog.msgbox("Installing  Pi-Hole")
        return "Installing  Pi-Hole"

    def __freshrss(self):
        #self.dialog.msgbox("Installing Fresh RSS")
        return "Installing Fresh RSS"

# 
def main():
    dialog = Dialog(dialog="dialog", DIALOGRC="dialogrc")

    wizard = Wizard(dialog)

    screens = [ 
        Welcome(dialog, wizard), 
        Automated(dialog, wizard),
        Advanced(dialog, wizard)
    ]

    try:
        while wizard.is_running:
            screens[wizard.active].display()
    
    except KeyboardInterrupt: # Handle Ctrl+C
        wizard.is_running = False

    
# Program entry point
if __name__ == "__main__":
    main()

# Clear the console on program finish
os.system('clear')