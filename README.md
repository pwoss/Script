# We moved everything to our own Gitea instance -> [git.pwoss.xyz](https://git.pwoss.xyz/PwOSS-Server/Installation)

# PwOSS - Server | Raspberry - Archlinux | ARM
### (Script)

__The image script isn't done yet.__

&nbsp;

## What's included?

[Raspberry Pi - Software](https://guideline.pwoss.xyz/basics/overview/sub-topic)  
(pwoss.xyz - link)

&nbsp;

## Download

### Raspberry Pi - Image
You can use the Raspberry Pi - Image from [www.seafile.pwoss.xyz](https://seafile.pwoss.xyz/d/1215a57671da473cadbe/?p=/1.%20Raspberry%20Pi/Arch%20Linux%20%7C%20ARM/PwOSS%20-%20Image/Latest&mode=list) (PW=PwOSS.xyz)

&nbsp;

### (Image Docu).md - File
For now, you have to use the .md file at [seafile.pwoss.xyz](https://seafile.pwoss.xyz/d/1215a57671da473cadbe/?p=/1.%20Raspberry%20Pi/Arch%20Linux%20%7C%20ARM/PwOSS%20-%20Image/Latest&mode=list) as well or at [GitHub](https://github.com/PwOSS/Documentation/blob/master/Raspberry/PwOSS%20-%20Server%20%7C%20Raspberry%20-%20Archlinux%20%7C%20ARM%20-%20(Image%20Docu).md) to get a running server at home.

&nbsp;

### Guideline

There is a [Guideline](https://guideline.pwoss.xyz/) to make it easier for you.

&nbsp;

## Script

The script will be based on the [.md](https://github.com/PwOSS/Documentation/blob/master/PwOSS%20-%20Server%20%7C%20Raspberry%20-%20Archlinux%20%7C%20ARM%20-%20(Image%20Docu).md) file.

&nbsp;

## Python

Created python script to help automate the process of setting up the PwOSS - Server.

&nbsp;

## Info
More information about __PwOSS - Privacy with Open Source Software__ at https://pwoss.xyz/.

&nbsp;
&nbsp;

## License
[GNU General Public License v3.0](https://github.com/PwOSS/scripts/blob/master/LICENSE)
